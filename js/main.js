/* Open the sidenav
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
} */

/* Close/hide the sidenav
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
} */

/* Adding About Information */
function about() {
    var strMessageIH = '<div class="row animated fadeIn"><div class="col-md-6 "><span class="sectionTitle">About</span><p class="lead">My name is Zaid Saeed. I am a Computer Science BS student at Indiana University Purdue University Indianapolis (IUPUI). </p><p class="lead"><a href="https://bitbucket.org/z_saeed/">bitbucket.org/z_saeed</a> || <a href="files/resume.pdf">Resume</a></p><p class="lead">Some languages that I have worked in include Java, C, C++, and Python. I also have worked with HTML5, CSS3, and Javascript. I have worked with frameworks like Bootstrap and often try to expand my knowledge with web development each time I create a website. I also worked with making templates for Wordpress and all the php coding that is involved in making a static website compatible with wordpress elements.</p><p class="lead">As I complete more projects, I will update this website with links to the projects that will be either hosted on the webserver or using a third party website like Github or Bitbucket.</p></div><div class="col-md-6"><div class="image"></div></div></div>';
    var elementIH = document.getElementById("innerContent");
    elementIH.innerHTML = strMessageIH;
}

function skill() {
    var strMessageIH = '<div class="row animated fadeIn"><div class="col-md-6"><canvas id="myChart" width="10" height="10"></canvas></div><div class="col-md-6"><span class="sectionTitle">Skills</span><p class="lead">A random 0 - 5 scale does not tell you much about how well I know each language. This is just a level of confidence that I have with each language. I have a lot to learn regarding each language.</p><p class="lead">After making numerous websites, I feel confident I can take design ideas and implement them using HTML and CSS. Regarding Javascript, I have not used it as much until recently and look forward to expanding my knowledge in that field. Java and C++ have been a core language in many of my Computer Science courses. While I find C one of my harder languages to grasp, I am working hard to ensure that I can know the language as well as possible.</p></div></div>';
    var elementIH = document.getElementById("innerContent");
    elementIH.innerHTML = strMessageIH;

    /*Bar Graph*/
    var ctx = document.getElementById("myChart");
    var myBarChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ["HTML", "CSS", "Javascript", "Java", "C++", "C"],
            datasets: [{
                label: 'Proficiency',
                data: [5, 4.5, 3, 4, 4, 2.5],
                backgroundColor: [
            'rgba(221,110,66, .7)',
            'rgba(221,110,66, .7)',
            'rgba(221,110,66, .7)',
            'rgba(221,110,66, .7)',
            'rgba(221,110,66, .7)',
            'rgba(221,110,66, .7)'
        ]
    }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
        }]
            }
        }
    });
}


function project() {
    var strMessageIH = '<div class="animated fadeIn"><span class="sectionTitle">Projects</span><p class="lead">BitBucket:<a href="https://bitbucket.org/z_saeed/"> bitbucket.org/z_saeed</a> Resume: <a href="files/resume.pdf">resume.pdf</a></p><hr class="style-four"><div class="row"><div class="col-sm-6"><h4>Website</h4><p class="lead"><a href="http://www.bayesianscientific.org/">bayesianscientific.org</a></p><p class="lead"><a href="https://cs.iupui.edu/~zasaeed/n341/">CSCI N341 Personal Webpage</a></p><p class="lead">I will update this website with links to the projects that will be either hosted on the webserver or using a third party website like Github or Bitbucket.</p></div><div class="col-sm-6" id="project"><h4>Projects</h4><p class="lead">I will update this website with links to the projects that will be either hosted on the webserver or using a third party website like Github or Bitbucket.</p></div></div></div>';
    var elementIH = document.getElementById("innerContent");
    elementIH.innerHTML = strMessageIH;
}
